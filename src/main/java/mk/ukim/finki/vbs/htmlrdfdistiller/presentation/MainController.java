package mk.ukim.finki.vbs.htmlrdfdistiller.presentation;

import mk.ukim.finki.vbs.htmlrdfdistiller.model.InputFormat;
import mk.ukim.finki.vbs.htmlrdfdistiller.model.OutputFormat;
import mk.ukim.finki.vbs.htmlrdfdistiller.service.ScraperService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin({"*", "localhost:3000"})
public class MainController {
    private final ScraperService scraperService;

    public MainController(ScraperService scraperService) {
        this.scraperService = scraperService;
    }

    @GetMapping
    public String getRdfFromUri(HttpServletRequest request,
                                @RequestParam(name = "url") String url,
                                @RequestParam(name = "outputFormat") String outputFormat) {

        OutputFormat outputFormatEnum;

        switch (outputFormat) {
            case "radRdfXml":
                outputFormatEnum = OutputFormat.RdfXml;
                break;
            case "radNTriples":
                outputFormatEnum = OutputFormat.NTriples;
                break;
            case "radJsonLd":
                outputFormatEnum = OutputFormat.JsonLd;
                break;
            default:
                outputFormatEnum = OutputFormat.Turtle;
                break;
        }
        System.out.println(outputFormat + " " + outputFormatEnum);
        String res = scraperService.getRdfFromUri(url, outputFormatEnum);
        System.out.println(res);
        return res;
    }
}
