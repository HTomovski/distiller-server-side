package mk.ukim.finki.vbs.htmlrdfdistiller.service;

import mk.ukim.finki.vbs.htmlrdfdistiller.model.InputFormat;
import mk.ukim.finki.vbs.htmlrdfdistiller.model.OutputFormat;

import java.util.List;

public interface ScraperService {
    String getRdfFromUri(String url, OutputFormat outputFormat);
}
