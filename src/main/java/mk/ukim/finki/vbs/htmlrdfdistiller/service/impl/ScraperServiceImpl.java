package mk.ukim.finki.vbs.htmlrdfdistiller.service.impl;

import com.github.jsonldjava.utils.JsonUtils;
import mk.ukim.finki.vbs.htmlrdfdistiller.model.InputFormat;
import mk.ukim.finki.vbs.htmlrdfdistiller.model.OutputFormat;
import mk.ukim.finki.vbs.htmlrdfdistiller.service.ScraperService;
import org.apache.any23.Any23;
import org.apache.any23.extractor.ExtractionException;
import org.apache.any23.extractor.microdata.MicrodataExtractor;
import org.apache.any23.extractor.microdata.MicrodataExtractorFactory;
import org.apache.any23.extractor.yaml.ElementsProcessor;
import org.apache.any23.filter.IgnoreAccidentalRDFa;
import org.apache.any23.filter.IgnoreTitlesOfEmptyDocuments;
import org.apache.any23.http.HTTPClient;
import org.apache.any23.source.DocumentSource;
import org.apache.any23.source.FileDocumentSource;
import org.apache.any23.source.HTTPDocumentSource;
import org.apache.any23.source.StringDocumentSource;
import org.apache.any23.writer.*;
import org.apache.commons.io.IOUtils;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.DatasetFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFWriter;
import org.apache.jena.rdf.model.impl.NTripleWriter;
import org.apache.jena.riot.*;
import org.apache.jena.riot.lang.JsonLDReader;
import org.apache.jena.riot.system.StreamRDFLib;
import org.apache.jena.sparql.util.Context;
import org.apache.jena.util.FileManager;
import org.eclipse.rdf4j.rio.helpers.RioFileTypeDetector;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Service
public class ScraperServiceImpl implements ScraperService {
    @Override
    public String getRdfFromUri(String url, OutputFormat outputFormat) {

        Model parsedModel = ModelFactory.createDefaultModel();

        Document jsoupDocument = null;

        try {
            jsoupDocument = Jsoup.connect(url).get();
        } catch (IOException e) {
            e.printStackTrace();
        }

        StringBuilder jsonLdContent = new StringBuilder();
        Elements scriptElements = null;
        if (jsoupDocument != null) {
            scriptElements = jsoupDocument.getElementsByAttributeValue("type", "application/ld+json");
        }

        if (scriptElements != null) {
            for (Element element : scriptElements)
                jsonLdContent.append(element.data());
        }
        String test = "\n" +
                "{\n" +
                "    \"@type\": \"WebPage\",\n" +
                "    \"name\": \"JSON-LD Examples, Snippets and Generator\",\n" +
                "    \"description\": \"I bet you wondered if this website had JSON-LD on it? \",\n" +
                "    \"publisher\": {\n" +
                "        \"@type\": \"ProfilePage\",\n" +
                "        \"name\": \"Our Site\"\n" +
                "    }\n" +
                "}";
        RDFParser parser = RDFParser.create().fromString(test).forceLang(Lang.JSONLD).build();
        parser.parse(parsedModel);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        switch (outputFormat) {
            case JsonLd:
                System.out.println("JsonLd");
                RDFDataMgr.write(baos, parsedModel, RDFFormat.JSONLD);
                break;

            case RdfXml:
                RDFDataMgr.write(baos, parsedModel, RDFFormat.RDFXML);
                break;

            case NTriples:
                RDFDataMgr.write(baos, parsedModel, RDFFormat.NTRIPLES);
                break;

            default:
                RDFDataMgr.write(baos, parsedModel, RDFFormat.TURTLE);
                break;
        }

        return baos.toString(StandardCharsets.UTF_8);
    }
}
