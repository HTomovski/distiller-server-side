package mk.ukim.finki.vbs.htmlrdfdistiller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@ServletComponentScan
@SpringBootApplication
public class HtmlRdfDistillerApplication {

    public static void main(String[] args) {
        SpringApplication.run(HtmlRdfDistillerApplication.class, args);
    }

}
