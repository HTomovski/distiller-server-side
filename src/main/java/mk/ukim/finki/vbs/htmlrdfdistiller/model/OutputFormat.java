package mk.ukim.finki.vbs.htmlrdfdistiller.model;

public enum OutputFormat {
    RdfXml,
    NTriples,
    Turtle,
    JsonLd
}
