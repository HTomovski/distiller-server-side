package mk.ukim.finki.vbs.htmlrdfdistiller.model;

public enum InputFormat {
    RDFa,
    Microdata,
    Turtle,
    JsonLd
}
